# Redisearch Course Image

In order to use this image, do a command like the one below.

```bash
docker run --name my_redis -p 6379:6379 -d registry.gitlab.com/jojomickymack/redisearch_image01:0.0.1
```

Then you can either use redis on port 6379 or have an interactive session with the container with the command below.

```bash
docker exec -it my_redis bash
```

Now you need to load the redisearch module, which is in a directory called '/module'. Start a redis-cli session and run the load module command.

```bash
redis-cli

module list

module load /module/redisearch.so
```

you should see 'OK' when loading the module, then checking the module list will return what's below.

```bash
1) 1) "name"
   2) "ft"
   3) "ver"
   4) (integer) 10600
```

Now you need to load the example dataset - exit the redis-cli session and go to /src/ru201/data directory and run this command.

```bash
node index.js --source ./General_Building_Permits.csv --connection ./connection.json --drop --totaldocs 121828
```

The dataset will be loaded. Verify that it's there by starting another redis-cli session and type 'ft.search permits garage'. It should return a pile of data.